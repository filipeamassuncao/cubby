//
//  UploadViewController.swift
//  CUbbY
//
//  Created by Filipe Assunção on 05/04/16.
//  Copyright © 2016 Filipe Assunção. All rights reserved.
//

import Foundation
import MessageUI

class UploadViewController: BaseViewController, MFMailComposeViewControllerDelegate {
    
    override init() {
        
        super.init(nibName:"UploadViewController", bundle: nil)
        self.title = "Upload"
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        
        self.init()
    }
    
    @IBAction func emailPressed(sender: AnyObject) {
        
//        if let composer = TranferManager.sendEmail() {
//            
//            self.presentViewController(composer, animated: true, completion: nil)
//        }
//        PersistenceManager.sharedInstance.transferData()
    }
    
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }

}