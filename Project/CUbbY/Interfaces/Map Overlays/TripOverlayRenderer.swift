//
//  TripOverlay.swift
//  CUbbY
//
//  Created by Filipe Assunção on 21/06/16.
//  Copyright © 2016 Filipe Assunção. All rights reserved.
//

import Foundation

class TripOverlayRenderer : MKPolylineRenderer {
    
    init(polyline: MKPolyline, color:UIColor) {
        
        super.init(polyline: polyline)
        self.strokeColor = color
        self.lineWidth = 3.0
    }
    
    override init(overlay: MKOverlay) {
        
        super.init(overlay: overlay)
    }
    
}
