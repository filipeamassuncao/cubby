//
//  ActivityAnnotation.swift
//  CUbbY
//
//  Created by Filipe Assunção on 15/04/16.
//  Copyright © 2016 Filipe Assunção. All rights reserved.
//

import UIKit
import MapKit

class ActivityAnnotation: NSObject, MKAnnotation {
    
    @objc var coordinate: CLLocationCoordinate2D
    var type: ActivityType
    
    init(coordinate: CLLocationCoordinate2D, type: ActivityType) {
        self.coordinate = coordinate
        self.type = type
    }

}
