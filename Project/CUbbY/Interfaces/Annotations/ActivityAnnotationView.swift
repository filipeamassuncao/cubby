//
//  ActivityAnnotationView.swift
//  CUbbY
//
//  Created by Filipe Assunção on 15/04/16.
//  Copyright © 2016 Filipe Assunção. All rights reserved.
//

import UIKit
import MapKit

class ActivityAnnotationView: MKAnnotationView {
    
    // Called when drawing the AttractionAnnotationView
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        let annotation = self.annotation as! ActivityAnnotation
        
        let rawImage = UIImage(named: annotation.type.rawValue)
        image = rawImage?.resizeImage(0.25)
    }

    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
    }

}
