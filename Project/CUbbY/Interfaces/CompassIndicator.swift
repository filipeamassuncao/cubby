//
//  CompassIndicator.swift
//  CUbbY
//
//  Created by Filipe Assunção on 21/03/16.
//  Copyright © 2016 Filipe Assunção. All rights reserved.
//

import UIKit


let kConstantDefaultImage = "notched-compass"


class CompassIndicator: UIImageView {
    
    init() {
        
        super.init(image: UIImage(named: kConstantDefaultImage))
    }

    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        self.image = UIImage(named: kConstantDefaultImage)
    }
    
    func orientToAngle(angleTo:Double) {

        let transform = CGAffineTransformMakeRotation(CGFloat(angleTo))
        
        self.transform = transform
    }
    
}
