//
//  MainMenuTableCell.swift
//  CUbbY
//
//  Created by Filipe Assunção on 11/04/16.
//  Copyright © 2016 Filipe Assunção. All rights reserved.
//

import UIKit

class MainMenuTableCell: UITableViewCell {
    
    
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellLabel: UILabel!
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        self.configureViews()
    }
    
    private func configureViews() {
        
        self.backgroundColor = UIColor.clearColor()
    }
    
    func configureCell(text:String, image:UIImage) {
        
        if let _ = cellLabel {
            
            cellLabel.text = text
        }
        
        if let _ = cellImage {
            
            cellImage.image = image
        }
    }
    
    static func defaultHeight() -> CGFloat {
     
        return 49
    }
}
