//
//  UIImage+Resize.swift
//  CUbbY
//
//  Created by Filipe Assunção on 20/04/16.
//  Copyright © 2016 Filipe Assunção. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    
    func resizeImage(scaleFactor:CGFloat) -> UIImage {
        
        let size = CGSizeApplyAffineTransform(self.size, CGAffineTransformMakeScale(scaleFactor, scaleFactor))
        let hasAlpha = true
        let scale: CGFloat = 0.0 // Automatically use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(size, !hasAlpha, scale)
        self.drawInRect(CGRect(origin: CGPointZero, size: size))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return scaledImage
    }
    
}