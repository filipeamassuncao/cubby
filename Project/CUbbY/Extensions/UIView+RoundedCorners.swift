//
//  UIView+RoundedCorners.swift
//  CUbbY
//
//  Created by Filipe Assunção on 12/04/16.
//  Copyright © 2016 Filipe Assunção. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func roundCorners(corners:UIRectCorner, radius: CGFloat) {
        
        let rectShape = CAShapeLayer()
        rectShape.bounds = self.frame
        rectShape.position = self.center
        rectShape.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius)).CGPath
        self.layer.masksToBounds = true
        self.layer.mask = rectShape
    }
}