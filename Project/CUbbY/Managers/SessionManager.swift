//
//  SessionManager.swift
//  CUbbY
//
//  Created by Filipe Assunção on 30/05/16.
//  Copyright © 2016 Filipe Assunção. All rights reserved.
//

import Foundation

class SessionManager {
    
    static var sessionNumber:Int {
        
        get {
            
            return PrefsManager.AppSessionNumber()
        }
        
        set {
            
            PrefsManager.SaveAppSessionNumber(newValue)
        }
    }
}