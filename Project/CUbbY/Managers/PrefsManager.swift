//
//  PrefsManager.swift
//  CUbbY
//
//  Created by Filipe Assunção on 21/03/16.
//  Copyright © 2016 Filipe Assunção. All rights reserved.
//

import Foundation

//Default Values
let kConstantDefaultsHeadingFilter:Double = 5
let kConstantDefaultsTimerInterval:Double = 5
let kConstantDefaultsLocationFilter:Double = 10
let kConstantActivityType:ActivityType = ActivityType.ActivityTypeRunning
let kConstantDefaultsPersistData:Bool = true
let kConstantDefaultsSessionNumber:Int = 1

//UserDefaults Keys
let kUserDefaultsHeadingFilter:String = "kUserDefaultsHeadingFilter"
let kUserDefaultsLocationFilter:String = "kUserDefaultsLocationFilter"
let kUserDefaultsTimerInterval:String = "kUserDefaultsTimerInterval"
let kUserDefaultsActivityType:String = "kUserDefaultsActivityType"
let kUserDefaultsPersistData:String = "kUserDefaultsPersistData"
let kUserDefaultsSessionNumber:String = "kUserDefaultsSessionNumber"

class PrefsManager {
    
    let defaults = NSUserDefaults.standardUserDefaults()

    class func locationHeadingFilter() -> Double {
        
        let defaults = NSUserDefaults.standardUserDefaults()
        if let obj = defaults.objectForKey(kUserDefaultsHeadingFilter) as? Double
        {
            return obj
        }
        savePrefs(kConstantDefaultsHeadingFilter, forKey: kUserDefaultsHeadingFilter)
        return kConstantDefaultsHeadingFilter
    }
    
    class func locationDistanceFilter() -> Double {
        
        let defaults = NSUserDefaults.standardUserDefaults()
        if let obj = defaults.objectForKey(kUserDefaultsLocationFilter) as? Double
        {
            return obj
        }
        savePrefs(kConstantDefaultsLocationFilter, forKey: kUserDefaultsLocationFilter)
        return kConstantDefaultsLocationFilter
    }
    
    class func GeoStateTimerInterval() -> Double {
        
        let defaults = NSUserDefaults.standardUserDefaults()
        if let obj = defaults.objectForKey(kUserDefaultsTimerInterval) as? Double
        {
            return obj
        }
        savePrefs(kConstantDefaultsTimerInterval, forKey: kUserDefaultsTimerInterval)
        return kConstantDefaultsTimerInterval
    }
    
    class func PersistData() -> Bool {
        
        let defaults = NSUserDefaults.standardUserDefaults()
        if let obj = defaults.objectForKey(kUserDefaultsPersistData) as? Bool
        {
            return obj
        }
        savePrefs(kConstantDefaultsPersistData, forKey: kUserDefaultsPersistData)
        return kConstantDefaultsPersistData
    }
    
    class func GeoStateActivityType() -> ActivityType {
        
        let defaults = NSUserDefaults.standardUserDefaults()
        if let obj = defaults.objectForKey(kUserDefaultsTimerInterval) as? String
        {
            return ActivityType.selectedType(obj)
        }
        savePrefs(kConstantDefaultsTimerInterval, forKey: kUserDefaultsTimerInterval)
        return kConstantActivityType
    }
    
    class func AppSessionNumber() -> Int {
        
        let defaults = NSUserDefaults.standardUserDefaults()
        if let obj = defaults.objectForKey(kUserDefaultsSessionNumber) as? Int
        {
            return obj
        }
        savePrefs(kConstantDefaultsSessionNumber, forKey: kUserDefaultsSessionNumber)
        return kConstantDefaultsSessionNumber
    }
    
    class func SaveAppSessionNumber(session:Int) {
        
        return savePrefs(session, forKey: kUserDefaultsSessionNumber)
    }
    
    private class func savePrefs(value: AnyObject?, forKey defaultName: String) {
        
        if let _ = value {
            
            let defaults = NSUserDefaults.standardUserDefaults()
            defaults.setObject(value, forKey:defaultName)
        }
    }
    
    class func loadPrefs(value: AnyObject?, forKey defaultName: String) -> AnyObject? {
        
        if let _ = value {
            
            let defaults = NSUserDefaults.standardUserDefaults()
            return defaults.objectForKey(kUserDefaultsTimerInterval)
        }
        return nil
    }
    
    private class func clearPrefs() {
        
        for key in Array(NSUserDefaults.standardUserDefaults().dictionaryRepresentation().keys) {
            NSUserDefaults.standardUserDefaults().removeObjectForKey(key)
        }
    }
}