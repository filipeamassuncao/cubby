//
//  CoreLocationManager.swift
//  CUbbY
//
//  Created by Filipe Assunção on 18/03/16.
//  Copyright © 2016 Filipe Assunção. All rights reserved.
//

import Foundation
import CoreLocation
import CoreMotion
import UIKit

enum ActivityType : String {
    
    case ActivityTypeWalking = "WALKING"
    case ActivityTypeRunning = "RUNNING"
    case ActivityTypeSitting = "SITTING"
    case ActivityTypeClimbing = "CLIMBING"
    case ActivityTypeBiking = "BIKING"
    case ActivityTypeDriving = "DRIVING"
    case ActivityTypeNone = "NONE"
    
    
    static func allValues() -> [ActivityType] {
        
        return [ActivityType.ActivityTypeWalking,
            ActivityType.ActivityTypeRunning,
            ActivityType.ActivityTypeSitting,
            ActivityType.ActivityTypeClimbing,
            ActivityType.ActivityTypeBiking,
            ActivityType.ActivityTypeDriving]
    }
    
    static func allValuesString() -> [String] {
        
        return [ActivityType.ActivityTypeWalking.rawValue,
            ActivityType.ActivityTypeRunning.rawValue,
            ActivityType.ActivityTypeSitting.rawValue,
            ActivityType.ActivityTypeClimbing.rawValue,
            ActivityType.ActivityTypeBiking.rawValue,
            ActivityType.ActivityTypeDriving.rawValue]
    }
    
    static func selectedType(type:String) -> ActivityType {
        
        switch(type) {
            
        case "WALKING": return ActivityType.ActivityTypeWalking;
        case "RUNNING": return ActivityType.ActivityTypeRunning;
        case "CLIMBING": return ActivityType.ActivityTypeClimbing;
        case "SITTING": return ActivityType.ActivityTypeSitting;
        case "BIKING": return ActivityType.ActivityTypeBiking;
        case "DRIVING": return ActivityType.ActivityTypeDriving;
            
        default: return ActivityType.ActivityTypeNone
            
        }
    }
    
    static func indexForActivityType(type:String) -> Int {
        
        return ActivityType.allValuesString().indexOf(type)!
    }
    
    static func ActivityTypeForIndex(index:Int) -> ActivityType {
        
        return index >= ActivityType.allValues().count ? ActivityType.ActivityTypeNone : ActivityType.allValues()[index]
    }
}

@objc protocol GeoStateManagerDelegate {
    
    optional func updateToNewLocation(newlocation:CLLocation)
    optional func updateToNewLocation(newlocation:CLLocation, oldlocation:CLLocation)
    optional func updateToNewHeading(newlocation:CLHeading)
}

protocol GeoStateManagerDataCollectionDelegate {
    
    func collectedDataInCriteria(data:GeoStateData)
}

class GeoStateManager : NSObject, CLLocationManagerDelegate {
    
    let accuracyValues = [
        kCLLocationAccuracyBestForNavigation,
        kCLLocationAccuracyBest,
        kCLLocationAccuracyNearestTenMeters,
        kCLLocationAccuracyHundredMeters,
        kCLLocationAccuracyKilometer,
        kCLLocationAccuracyThreeKilometers]
    
    var locationManager:CLLocationManager
    var motionManager:CMMotionManager
    var delegate:GeoStateManagerDelegate?
    var dataCollectionDelegate:GeoStateManagerDataCollectionDelegate?
    var dataCollectionTimer:NSTimer
    var activity:ActivityType
    var persistData:Bool
    var timesUP:Bool = false
    
    override init() {
        
        self.locationManager = CLLocationManager()
        self.motionManager = CMMotionManager()
        self.dataCollectionTimer = NSTimer()
        self.activity = PrefsManager.GeoStateActivityType()
        self.persistData = PrefsManager.PersistData()
        super.init()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.headingFilter = PrefsManager.locationHeadingFilter()
        self.locationManager.distanceFilter = PrefsManager.locationDistanceFilter()
        UIDevice.currentDevice().proximityMonitoringEnabled = true;
        
    }
    
    func startUpdatingLocation() {
        
        self.locationManager.startUpdatingLocation()
    }
    
    func startUpdatingHeading() {
        
        self.locationManager.startUpdatingHeading()
    }
    
    func startUpdatingMotion() {
        
        self.motionManager.startAccelerometerUpdates()
        self.motionManager.startGyroUpdates()
        UIDevice.currentDevice().proximityMonitoringEnabled = true
        startTimer()
    }
    
    
    func stopAllUpdates() {
        
        stopUpdatingLocation()
        stopUpdatingMotion()
        stopUpdatingHeading()
        stopTimer()
    }
    
    func startAllUpdates() {
        
        startUpdatingLocation()
        startUpdatingMotion()
        startUpdatingHeading()
        startTimer()
    }
    
    func stopUpdatingMotion() {
        
        self.motionManager.stopAccelerometerUpdates()
        self.motionManager.stopGyroUpdates()
        UIDevice.currentDevice().proximityMonitoringEnabled = false
        stopTimer()
    }
    
    func stopUpdatingLocation() {
        
        self.locationManager.stopUpdatingLocation()
    }
    
    func stopUpdatingHeading() {
        
        self.locationManager.stopUpdatingHeading()
    }
    
    func updateAccuracy(index:Int) {
        
        self.locationManager.desiredAccuracy = accuracyValues[index]
    }
    
    func locationManager(manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        
//        NSLog("Actual heading : %@", newHeading.magneticHeading)
        if let _ = delegate {
            
            delegate!.updateToNewHeading!(newHeading)
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateToLocation newLocation: CLLocation, fromLocation oldLocation: CLLocation) {
        
        NSLog("Actual location : %@", newLocation.description)
        if let _ = delegate {
            
            //            delegate!.updateToNewLocation!(newLocation)
            delegate!.updateToNewLocation!(newLocation, oldlocation:oldLocation)
            
            if newLocation.distanceFromLocation(oldLocation) > PrefsManager.locationDistanceFilter() {
                
                self.updateDelegateCollectedData()
                
            }
        }
    }
    
    func updateDelegateCollectedData() {
        
        let data = self.getMeFullData()!
        NSLog("PERSIST THIS DATA : \n\(data.printMe())")
        self.startTimer()
        if let _ = dataCollectionDelegate {
            
            dataCollectionDelegate?.collectedDataInCriteria(data)
            
        }
        if self.persistData {
            
            DBManager.sharedInstance.insertGeoStateDataInDB(data)
        }
    }
    
    func getMeFullData() -> GeoStateData? {
        
        
        if let _ = self.motionManager.accelerometerData?.acceleration {
            
            if let _ = self.motionManager.gyroData?.rotationRate {
                
                return GeoStateData(lat:(self.locationManager.location?.coordinate.latitude)!,
                    lon: (self.locationManager.location?.coordinate.longitude)!,
                    alt: (self.locationManager.location?.altitude)!,
                    time: GeoStateData.getDate(),
                    xacc: (self.motionManager.accelerometerData?.acceleration.x)!,
                    yacc: (self.motionManager.accelerometerData?.acceleration.y)!,
                    zacc: (self.motionManager.accelerometerData?.acceleration.z)!,
                    xgyro: (self.motionManager.gyroData?.rotationRate.x)!,
                    ygyro: (self.motionManager.gyroData?.rotationRate.y)!,
                    zgyro: (self.motionManager.gyroData?.rotationRate.z)!,
                    opt: UIDevice.currentDevice().proximityState ? 0 : 1,
                    act: activity.rawValue)
            }
            return GeoStateData(lat:(self.locationManager.location?.coordinate.latitude)!,
                lon: (self.locationManager.location?.coordinate.longitude)!,
                alt: (self.locationManager.location?.altitude)!,
                time: GeoStateData.getDate(),
                xacc: (self.motionManager.accelerometerData?.acceleration.x)!,
                yacc: (self.motionManager.accelerometerData?.acceleration.y)!,
                zacc: (self.motionManager.accelerometerData?.acceleration.z)!,
                xgyro: 0,
                ygyro: 0,
                zgyro: 0,
                opt: 0,
                act: activity.rawValue)
        }
        
        return GeoStateData(lat:(self.locationManager.location?.coordinate.latitude)!,
            lon: (self.locationManager.location?.coordinate.longitude)!,
            alt: (self.locationManager.location?.altitude)!,
            time: GeoStateData.getDate(),
            xacc: 0,
            yacc: 0,
            zacc: 0,
            xgyro: 0,
            ygyro: 0,
            zgyro: 0,
            opt: 0,
            act: activity.rawValue)
        
    }
    
    //MARK: Timer Methods
    
    private func startTimer() {
        
        self.dataCollectionTimer.invalidate()
        self.dataCollectionTimer = NSTimer.scheduledTimerWithTimeInterval(PrefsManager.GeoStateTimerInterval(), target: self, selector: "updateDelegateCollectedData", userInfo: nil, repeats: true)
        
    }
    
    private func stopTimer() {
        
        self.dataCollectionTimer.invalidate()
    }
}
