//
//  DBManager.swift
//  CUbbY
//
//  Created by Filipe Assunção on 12/05/16.
//  Copyright © 2016 Filipe Assunção. All rights reserved.
//

import UIKit

// DB Identifiers

let kDBFileName                           = "/spatialDBName3.sqlite"
let kDBFileType                           = "sqlite"

// SQLite

let kSQLiteSelectAllFromTable             = "SELECT longitude, latitude, altitude, date, x_acc, y_acc, z_acc, x_gyro, y_gyro, z_gyro, proximity, activity FROM test"


class DBManager: NSObject {
    
    static let sharedInstance = DBManager()
    var spatialDB:SpatialDatabase
    
    static var filePath:String {
        
        get {
            
            let paths = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)
            let documentDir = paths[0]
            let filePath = documentDir.stringByAppendingString(kDBFileName)
            return filePath
        }
    }
    
    var fileExists:Bool {
        
        get {
            
            return NSFileManager.defaultManager().fileExistsAtPath(DBManager.filePath);
        }
    }
    
    private override init() {
        
        self.spatialDB = SpatialDatabase()
        super.init()
        
        if fileExists {
            
            #if DEBUG
            resetDatabase()
            #else
            openDatabase()
            #endif
            
        } else {
            
            createDatabase()
        }
    }
    
    func resetDatabase() {
        
        NSLog("[RESETING DATABASE]")
        do {
            
            try NSFileManager.defaultManager().removeItemAtPath(DBManager.filePath)
        }
        catch let error as NSError {
            
            NSLog("[ERROR REMOVING DATABASE] :: \(error)")
            return
        }
        
        NSLog("[PREVIOUS DATABASE REMOVED]")
        createDatabase()
    }
    
    func createDatabase() {
        
        NSLog("[CREATING DATABASE]")
        openDatabase()
        NSLog("[INITIATING SPATIAL METADATA]")
        self.spatialDB.executeStatements("SELECT InitSpatialMetaData();")
        NSLog("[ENABLING FOREIGN KEYS]")
        self.spatialDB.executeStatements("PRAGMA foreign_keys = 1;")
        self.createTables()
        NSLog("[ANALYZE]")
        self.spatialDB.executeStatements("ANALYZE;")

    }
    
    func openDatabase() {
    
        NSLog("[OPENNING DATABASE]")
        self.spatialDB = SpatialDatabase(path:DBManager.filePath)
        self.spatialDB.open()
    }
    
    func createTables() {
        
        NSLog("[CREATING TABLES]")
        SQLHelper.createSessionsTable(self.spatialDB)
        SQLHelper.createGeoStateTable(self.spatialDB)
        SQLHelper.createRoutesTable(self.spatialDB)
        SQLHelper.initVirtualTablesFromCSV(self.spatialDB)
    }
    
    func getAllGeoStateEntries() -> [NSDictionary] {
        
        var dictionary = [NSDictionary]()
        let rs = SQLHelper.getGeoStateEntries(self.spatialDB)
        while (rs.next()) {
            
            dictionary.append(rs.resultDictionary())
        }
        return dictionary
    }
    
    func getAllSessionEntries() -> [NSDictionary] {
        
        var dictionary = [NSDictionary]()
        let rs = SQLHelper.getSessionEntries(self.spatialDB)
        while (rs.next()) {
            
            dictionary.append(rs.resultDictionary())
        }
        return dictionary
    }
    
    func getAllRouteEntries() -> [NSDictionary] {
        
        var dictionary = [NSDictionary]()
        let rs = SQLHelper.getRoutesEntries(self.spatialDB)
        while (rs.next()) {
            
            dictionary.append(rs.resultDictionary())
        }
        return dictionary
    }
    
    func getAllGeoStateEntriesAsCLLocations() -> [CLLocation] {
        
        var dictionary = [CLLocation]()
        let rs = SQLHelper.getGeoStateEntries(self.spatialDB)
        while (rs.next()) {
            
            let lat = CLLocationDegrees(rs.doubleForColumn("latitude"))
            let lon = CLLocationDegrees(rs.doubleForColumn("longitude"))
            let alt = CLLocationDegrees(rs.doubleForColumn("altitude"))
            let cord = CLLocationCoordinate2D(latitude: lat, longitude: lon)
            let loc = CLLocation(coordinate: cord, altitude: alt, horizontalAccuracy: 0, verticalAccuracy: 0, timestamp:NSDate())
            dictionary.append(loc)
        }
        return dictionary
    }
    
    func getAllGeoStateEntriesAsShapeKit() -> [ShapeKitPoint] {
        
        var dictionary = [ShapeKitPoint]()
        let rs = SQLHelper.getGeoStateEntries(self.spatialDB)
        while (rs.next()) {
            
            //            let lat = CLLocationDegrees(rs.doubleForColumn("latitude"))
            //            let lon = CLLocationDegrees(rs.doubleForColumn("longitude"))
            if let xx = rs.objectForColumnName("geom_point") as! ShapeKitPoint! {
                
                dictionary.append(xx)
            }
        }
        return dictionary
    }
    
    func getSenseMyCityAsCLLocations() -> [CLLocation] {
        
        var dictionary = [CLLocation]()
        let rs = SQLHelper.getSenseMyCityCoimbraGPS(self.spatialDB)
        while (rs.next()) {
            
            let lat = CLLocationDegrees(rs.doubleForColumn("lat"))
            let lon = CLLocationDegrees(rs.doubleForColumn("lon"))
            let alt = CLLocationDegrees(rs.doubleForColumn("alt"))
//            if let xx = rs.objectForColumnName("geom_point") as! ShapeKitPoint! {
//                
//                dictionary.append(xx)
//            }
            let cord = CLLocationCoordinate2D(latitude: lat, longitude: lon)
            let loc = CLLocation(coordinate: cord, altitude: alt, horizontalAccuracy: 0, verticalAccuracy: 0, timestamp:NSDate())
            dictionary.append(loc)
        }
        return dictionary
    }
    
    func getSenseMyCityAltitudes() -> ([CLLocation], [Double]) {
        
        var dictionary = [CLLocation]()
        var altitudes = [Double]()
        let rs = SQLHelper.getSenseMyCityCoimbraGPS(self.spatialDB)
        while (rs.next()) {
            
            let lat = CLLocationDegrees(rs.doubleForColumn("lat"))
            let lon = CLLocationDegrees(rs.doubleForColumn("lon"))
            let alt = CLLocationDegrees(rs.doubleForColumn("alt"))
            //            if let xx = rs.objectForColumnName("geom_point") as! ShapeKitPoint! {
            //
            //                dictionary.append(xx)
            //            }
            let loc = CLLocation(latitude: lat, longitude: lon)
            dictionary.append(loc)
            altitudes.append(alt)
        }
        return (dictionary, altitudes)
    }
    
    func getNumberOfSessions() -> Int {
        
        return SQLHelper.getNumberOfTotalSessions(self.spatialDB)
    }
    
    func getNumberOfGeoStates() -> Int {
        
        return SQLHelper.getNumberOfTotalGeoStates(self.spatialDB)
    }
    
    func getNumberOfRoutes() -> Int {
        
        return SQLHelper.getNumberOfTotalRoutes(self.spatialDB)
    }
    
    func insertGeoStateDataInDB(data:GeoStateData) -> Bool {
        
        NSLog("[INSERTING DATA]")
        if SQLHelper.insertGeoStatePoint(self.spatialDB, data: data) {
            
            NSLog("[DATA INSERTED]")
            return true
        }
        NSLog("[ERROR INSERTING DATA] :: ")
        return false
    }
    
    func insertNewSessionInDB(start_time:NSDate) {
        
        NSLog("[INSERTING NEW SESSION]")
        SQLHelper.insertSession(start_time, database: self.spatialDB)
    }
    
    func updateSessionEndTime(session:Int, end_time:NSDate) -> Bool {
        
        NSLog("[INSERTING SESSION END_TIME]")
        return SQLHelper.updateSessionEndTime(session, end_time: end_time, database: self.spatialDB)
    }
    
    func getGeoStateDataForActivity(activityType:ActivityType) -> [GeoStateData] {
        
        var resultArray:[GeoStateData] = []
        let rs = SQLHelper.getAllGeoStatesByActivity(activityType, database: self.spatialDB)
        while rs.next() {
            
            let state = GeoStateData(lat: rs.doubleForColumn("latitude"), lon: rs.doubleForColumn("longitude"), alt: rs.doubleForColumn("altitude"), time: rs.dateForColumn("date"), xacc: rs.doubleForColumn("x_acc"), yacc: rs.doubleForColumn("y_acc"), zacc: rs.doubleForColumn("z_acc"), xgyro: rs.doubleForColumn("x_gyro"), ygyro: rs.doubleForColumn("y_gyro"), zgyro: rs.doubleForColumn("z_gyro"), opt: rs.doubleForColumn("proximity"), act: rs.stringForColumn("activity"))
            
            resultArray.append(state)
        }
        return resultArray
    }
    
    func getGeoStateDataForActivityAsCLLocations(activityType:ActivityType) -> [CLLocation] {
        
        var resultArray:[CLLocation] = []
        let rs = SQLHelper.getAllGeoStatesByActivity(activityType, database: self.spatialDB)
        while (rs.next()) {
            
            let lat = CLLocationDegrees(rs.doubleForColumn("latitude"))
            let lon = CLLocationDegrees(rs.doubleForColumn("longitude"))
            //            if let xx = rs.objectForColumnName("geom_point") as! ShapeKitPoint! {
            //
            //                dictionary.append(xx)
            //            }
            let loc = CLLocation(latitude: lat, longitude: lon)
            resultArray.append(loc)
        }
        return resultArray
    }
    
    func getFreguesiasFromCoimbraAsShapeKit() -> [ShapeKitPolygon] {
        
        return SQLHelper.getFreguesiasFromCoimbraAsShape(self.spatialDB)
    }
    
    func getNumeroFreguesiasFromDistrict(district:String) -> String {
        
        return SQLHelper.getNumeroFreguesiasByDistrict(self.spatialDB, district: district)
    }
    
    func getAreaFreguesiasFromDistrict(district:String) -> String {
        
        return SQLHelper.getAreaFreguesiaByDistrict(self.spatialDB, district:district)
    }
    
    func getSMCSessionWithMorePoints() -> (session:Int, number:Int) {
        
        return SQLHelper.getSMCSessionWithMorePoints(self.spatialDB)
    }
    
    func getGeoStateActivityWithMorePoints() -> (activity:String, number:Int) {
        
        return SQLHelper.getActivityWithMorePoints(self.spatialDB)
    }
    
    func getRoutesShapesBySession() -> [ShapeKitPolyline] {
        
        return SQLHelper.getRoutesBySession(self.spatialDB)
    }
    
    func getRoutesShapesBySessionAsWKT() -> [String] {
        
        return SQLHelper.getRoutesBySessionAsWKT(self.spatialDB)
    }
}
