//
//  PersistenceManager.swift
//  CUbbY
//
//  Created by Filipe Assunção on 31/03/16.
//  Copyright © 2016 Filipe Assunção. All rights reserved.
//

//import Foundation
//import CHCSVParser
//
//let kFileName                           = "/a21210365_.csv"
//
//let kFileManagerXACCField               = "x_acc"
//let kFileManagerYACCField               = "y_acc"
//let kFileManagerZACCField               = "z_acc"
//let kFileManagerXGYROField              = "x_gyro"
//let kFileManagerYGYROField              = "y_gyro"
//let kFileManagerZGYROField              = "z_gyro"
//let kFileManagerLatitudeField           = "lat"
//let kFileManagerLongitudeField          = "lng"
//let kFileManagerAltitudeField           = "alt"
//let kFileManagerTimeField               = "time"
//let kFileManagerOptionalField           = "Optional Sensor"
//let kFileManagerActivityField           = "activity"
//
//class PersistenceManager: NSObject, CHCSVParserDelegate {
//    
//    static let sharedInstance = PersistenceManager()
//    
//    let kNumberOfColunms        = 10;
//    
//    var fileManager:NSFileManager
//    var csvWriter:CHCSVWriter
//    lazy var csvParser:CHCSVParser = CHCSVParser()
//    
//    lazy var currentParsingLine:[String] = []
//    lazy var currentParsingDocument:NSMutableArray = []
//    
//    static var filePath:String {
//        
//        get {
//            
//            let paths = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)
//            let documentDir = paths[0]
//            let filePath = documentDir.stringByAppendingString(kFileName)
//            return filePath
//        }
//    }
//    
//    var fileExists:Bool {
//        
//        get {
//            
//            return NSFileManager.defaultManager().fileExistsAtPath(PersistenceManager.filePath);
//        }
//    }
//    
//    lazy var isOpenWriteStream:Bool = false
//    lazy var isFileSetUp:Bool = false
//    
//    private func setupParser() -> CHCSVParser {
//        
//        let parser = CHCSVParser(contentsOfCSVURL: NSURL(fileURLWithPath:PersistenceManager.filePath))
//        parser.delegate = self
//        return parser
//    }
//    
//    private override init() {
//        
//        fileManager = NSFileManager.defaultManager()
//        csvWriter = CHCSVWriter()
//        super.init()
////        self.setupFileManager()
//    }
//    
//    private func setupFile() {
//        
//        if !isFileSetUp {
//            
//            if !self.fileExists {
//                
//                fileManager.createFileAtPath(PersistenceManager.filePath, contents: nil, attributes: nil)
//                initCSVFile()
//                self.isFileSetUp = true
//                
//            } else {
//                
//                self.csvParser = self.setupParser()
//                self.isFileSetUp = true
//                self.csvParser.parse()
//            }
//            
//        }
//    }
//    
//    private func initCSVFile() {
//        
//        if !self.isOpenWriteStream {
//            
//            self.isOpenWriteStream = true
//            self.csvWriter = CHCSVWriter.init(forWritingToCSVFile: PersistenceManager.filePath)
//        }
//        
//        csvWriter.writeField(kFileManagerLatitudeField)
//        csvWriter.writeField(kFileManagerLongitudeField)
//        csvWriter.writeField(kFileManagerAltitudeField)
//        csvWriter.writeField(kFileManagerTimeField)
//        csvWriter.writeField(kFileManagerXACCField)
//        csvWriter.writeField(kFileManagerYACCField)
//        csvWriter.writeField(kFileManagerZACCField)
//        csvWriter.writeField(kFileManagerXGYROField)
//        csvWriter.writeField(kFileManagerYGYROField)
//        csvWriter.writeField(kFileManagerZGYROField)
//        csvWriter.writeField(kFileManagerOptionalField)
//        csvWriter.writeField(kFileManagerActivityField)
//        
//        csvWriter.finishLine()
//
//    }
//    
//    func closeWriteStream() {
//        
//        if self.isOpenWriteStream {
//            
//            self.isOpenWriteStream = false
//            self.csvWriter.closeStream()
//        }
//    }
//    
//    func openWriteStream() {
//        
//        if !self.isFileSetUp {
//            
//            self.setupFile()
//        }
//        if !self.isOpenWriteStream {
//            
//            self.isOpenWriteStream = true
//            self.csvWriter = CHCSVWriter.init(forWritingToCSVFile: PersistenceManager.filePath)
//        }
//    }
//    
//    func removeExistingFiles() {
//        
//        if self.fileExists {
//            
//            self.closeWriteStream()
//
//            do {
//                
//                try fileManager.removeItemAtPath(PersistenceManager.filePath)
//                self.isFileSetUp = false
//            }
//            catch let error as NSError {
//                
//                print("Ooops! Something went wrong: \(error)")
//            }
//        }
//    }
//    
//    private func writeNewLine(newLineValues:NSArray) {
//        
//        self.csvWriter.writeLineOfFields(newLineValues)
//    }
//    
//    func writeGeoStateDataInFile(geoState:GeoStateData) {
//        
//        openWriteStream()
//        
//        csvWriter.writeField(geoState.latitude)
//        csvWriter.writeField(geoState.longitude)
//        csvWriter.writeField(geoState.altitude)
//        csvWriter.writeField(geoState.timestamp)
//        csvWriter.writeField(geoState.x_acc)
//        csvWriter.writeField(geoState.y_acc)
//        csvWriter.writeField(geoState.z_acc)
//        csvWriter.writeField(geoState.x_gyro)
//        csvWriter.writeField(geoState.y_gyro)
//        csvWriter.writeField(geoState.z_gyro)
//        csvWriter.writeField(geoState.optional)
//        csvWriter.writeField(geoState.activity)
//        
//        self.csvWriter.finishLine()
//    }
//    
//    func transferData() {
////        
//        if self.fileExists {
//            
//            self.closeWriteStream()
//            TranferManager.sharedInstance.tranferGeoStateDataToOmegaISEC(PersistenceManager.filePath)
//            
//            self.removeExistingFiles()
//        }
//    }
//    
//    func parserDidBeginDocument(parser: CHCSVParser!) {
//        
//        self.currentParsingDocument = NSMutableArray()
//    }
//    
//    func parser(parser: CHCSVParser!, didBeginLine recordNumber: UInt) {
//        
//        self.currentParsingLine = [String]()
//    }
//    
//    func parser(parser: CHCSVParser!, didReadField field: String!, atIndex fieldIndex: Int) {
//        
//        self.currentParsingLine.insert(field, atIndex: fieldIndex)
//    }
//    
//    func parser(parser: CHCSVParser!, didEndLine recordNumber: UInt) {
//        
//        self.currentParsingDocument.addObject(self.currentParsingLine)
//    }
//    
//    func parserDidEndDocument(parser: CHCSVParser!) {
//        
//        self.removeExistingFiles()
//        self.initCSVFile()
//        
//        if self.currentParsingDocument.count > 0 {
//            
//            for i in 0...self.currentParsingDocument.count-1 {
//                
//                if i == 0 {
//                    
//                    continue
//                }
//                
//                let line = self.currentParsingDocument[i]
//                self.writeGeoStateDataInFile(GeoStateData.buildWithOrderedArray(line as! [String]))
//            }
//        }
//        
//        self.currentParsingLine = []
//        self.currentParsingDocument = []
//    }
//}
