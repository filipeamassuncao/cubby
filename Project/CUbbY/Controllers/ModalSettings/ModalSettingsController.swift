//
//  ModalSettingsController.swift
//  CUbbY
//
//  Created by Filipe Assunção on 11/06/16.
//  Copyright © 2016 Filipe Assunção. All rights reserved.
//

import Foundation

@objc protocol ModalSettingsControllerDelegate {
    
    optional func updateDataToActivity(activity:Int)
    optional func showRoutesPolylines(showRoutes:Bool)
}

class ModalSettingsController : BaseViewController {
    
    var delegate:ModalSettingsControllerDelegate?
    var activityControl:UISegmentedControl
    
    @IBOutlet weak var activityContainer: UIView!
    
    override init() {
        
        self.activityControl = UISegmentedControl()
        super.init(nibName:"ModalSettingsController", bundle: nil)
        self.title = "Settings"
        
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        
        self.init()
    }
    
    override func viewDidLoad() {
     
        super.viewDidLoad()
        configureActivityTypeControl()
    }
    
    @IBAction func dismissConfigs(sender: AnyObject) {
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    private func configureActivityTypeControl() {
        
        let elements:[AnyObject]? = self.createActivityElements()
        
        
        self.activityControl = UISegmentedControl(items: elements)
        
        self.activityControl.frame = CGRect(x: 0, y: 0, width: self.activityContainer.frame.width, height: self.activityContainer.frame.height)
        
        self.activityControl.tintColor = UIColor.blackColor()
        self.activityContainer.backgroundColor = UIColor.clearColor()
        self.activityControl.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
        
//        self.activityControl.selectedSegmentIndex = ActivityType.indexForActivityType(self.locationManager.activity.rawValue)
        
        self.activityControl.tag = kActivitySegmentedControlTag
        
        self.activityControl.addTarget(self, action: "activityChanged", forControlEvents: .ValueChanged)
        
        self.activityContainer.addSubview(self.activityControl)
        
        self.view.layoutSubviews()
    }
    
    private func createActivityElements() -> [AnyObject]? {
        
        var imageElements:[AnyObject]? = []
        
        for activity in ActivityType.allValuesString() {
            
            imageElements?.append(UIImage(named: activity)!.resizeImage(0.25))
        }
        imageElements?.append(UIImage())
        return imageElements
    }
    
    func activityChanged() {
        
        //self.locationManager.activity = ActivityType.ActivityTypeForIndex(self.activityControl.selectedSegmentIndex)
        if let _ = delegate {
            
            delegate!.updateDataToActivity!(self.activityControl.selectedSegmentIndex)
            
        }
    }
    
    @IBAction func showTrips(sender: UISwitch) {
        
        if let _ = delegate {
            
            delegate!.showRoutesPolylines!(sender.on)
        }
    }
    
}
