//
//  LocationViewController.swift
//  CUbbY
//
//  Created by Filipe Assunção on 11/03/16.
//  Copyright (c) 2016 Filipe Assunção. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

let kActivitySegmentedControlTag = 7

class LocationViewController:BaseViewController, GeoStateManagerDelegate, GeoStateManagerDataCollectionDelegate, MKMapViewDelegate {
    
    var locations = [CLLocation]()
    
    var locationManager: GeoStateManager
    
    var weights = [Double]()
    
    var polyline = MKPolyline()
    var polylineView = MKPolylineView()
    
    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet var contentView: UIView!
    
    @IBOutlet weak var disabledView: UIView!
    
    @IBOutlet weak var compassIndicator: CompassIndicator!
    
    @IBOutlet weak var overlayImage: UIImageView!
    
    @IBOutlet weak var detailsLeadingSpace: NSLayoutConstraint!
    
    @IBOutlet weak var detailsViewContainer: UIView!
    
    @IBOutlet weak var arrowContainer: UIView!
    @IBOutlet weak var arrowImageView: UIImageView!
    
    var activityControl:UISegmentedControl
    @IBOutlet weak var activityContainer: UIView!
    
    //infoLabels
    @IBOutlet weak var lngValue: UILabel!
    @IBOutlet weak var latValue: UILabel!
    @IBOutlet weak var altValue: UILabel!
    
    @IBOutlet weak var accxValue: UILabel!
    @IBOutlet weak var accyValue: UILabel!
    @IBOutlet weak var acczValue: UILabel!
    
    @IBOutlet weak var gyroxValue: UILabel!
    @IBOutlet weak var guroyValue: UILabel!
    @IBOutlet weak var gyrozValue: UILabel!
    
    //Actions
    @IBAction func enabledChanged(sender: UISwitch) {
        
        self.disabledView.hidden = sender.on
        self.startLocators(sender.on)
        self.arrowContainer.hidden = !sender.on
    }
    
    @IBAction func accuracyChanged(sender: UISegmentedControl) {
        
        locationManager.updateAccuracy(sender.selectedSegmentIndex)
    }
    
    @IBAction func drawerButton(sender: AnyObject) {
        
        UIView.animateWithDuration(0.7, delay: 0, options: .CurveEaseOut, animations: {
            
            if self.detailsLeadingSpace.constant < 0 {
                
                self.detailsViewContainer.hidden = false
                self.detailsLeadingSpace.constant = 0
                
            } else  {
                
                self.detailsLeadingSpace.constant = -78
                self.detailsViewContainer.hidden = true
            }
            }, completion: { finished in
                
        })
        
        UIView.animateWithDuration(0.2, animations: {
            
            self.arrowImageView.transform = CGAffineTransformMakeRotation((180.0 * CGFloat(M_PI)) / 180.0)
        })
    }
    
    override init() {
        
        self.locationManager = GeoStateManager()
        self.activityControl = UISegmentedControl()
        super.init(nibName:"LocationViewController", bundle: nil)
        self.locationManager.delegate = self
        self.locationManager.dataCollectionDelegate = self
        self.title = "Location"
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        
        self.init()
    }
    
    private func createActivityElements() -> [AnyObject]? {
        
        var imageElements:[AnyObject]? = []
        
        for activity in ActivityType.allValuesString() {
            
            imageElements?.append(UIImage(named: activity)!.resizeImage(0.25))
        }
        return imageElements
    }
    
    private func configureActivityTypeControl() {
        
        let elements:[AnyObject]? = self.createActivityElements()
        
        self.activityControl = UISegmentedControl(items: elements)
        
        self.activityControl.frame = CGRect(x: 0, y: 0, width: self.activityContainer.frame.width, height: self.activityContainer.frame.height)
        
        self.activityControl.tintColor = UIColor.blackColor()
        self.activityContainer.backgroundColor = UIColor.clearColor()
        self.activityControl.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.79)
        
        self.activityControl.selectedSegmentIndex = ActivityType.indexForActivityType(self.locationManager.activity.rawValue)
        
        self.activityControl.tag = kActivitySegmentedControlTag
        
        self.activityControl.addTarget(self, action: "activityChanged", forControlEvents: .ValueChanged)
        
        self.activityContainer.addSubview(self.activityControl)
        
        self.view.layoutSubviews()
    }
    
    func activityChanged() {
        
        self.locationManager.activity = ActivityType.ActivityTypeForIndex(self.activityControl.selectedSegmentIndex)
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.mapView.mapType = MKMapType.Hybrid;
//        self.mapView.showsCompass = true
        self.mapView.delegate = self
        self.detailsLeadingSpace.constant = -78
        
        //rounded Corners
        self.detailsViewContainer.hidden = true
        self.detailsViewContainer.roundCorners(UIRectCorner.TopRight, radius: 7)
        self.arrowContainer.roundCorners([UIRectCorner.BottomRight, UIRectCorner.TopRight], radius: 7)
        self.view.layoutSubviews()
        
        self.arrowContainer.hidden = true
        self.disabledView.hidden = false
        
        self.configureActivityTypeControl()
    }
    
    override func viewWillDisappear(animated: Bool) {
        
        super.viewWillDisappear(animated)
        
        if (self.disabledView.hidden) {
         
            self.startLocators(false)
        }
    }
    
    // MARK: - CLLocationManagerDelegate
    
    func updateToNewHeading(newlocation:CLHeading) {
        
        self.rotateNorthIndicatorTo(newlocation)
    }
    
    func updateToNewLocation(newlocation:CLLocation, oldlocation:CLLocation) {
        
        // Add another annotation to the map.
        
        NSLog("Actual location : %@", newlocation.description)
        // Also add to our map so we can remove old values later
        locations.append(newlocation)
        weights.append(newlocation.speed)
        // Remove values if the array is too big
        
//        if locations.count > 50 {
//            
//            let annotationToRemove = locations.first!
//            locations.removeFirst()
//            weights.removeFirst()
//            
//            // Also remove from the map
//            mapView.removeAnnotation(annotationToRemove)
//            
//        }
        
        if UIApplication.sharedApplication().applicationState == .Active {
            
            //setOverlayToMap()
            
            addActivityAnnotation(at: newlocation)
            addRoute()
            
        } else {
            
            NSLog("App is backgrounded. New location is %@", newlocation)
        }
    }
    
    func collectedDataInCriteria(data:GeoStateData) {
        
        updateDetailsViewLabels(data)
    }
    
    func rotateNorthIndicatorTo(angleTo:CLHeading) {
        
        let angle = -(angleTo.magneticHeading*3.14)/180
        
        self.compassIndicator.orientToAngle(angle)
    }
    
    func startLocators(start: Bool) {
        
        if start {
            
            self.locationManager.startAllUpdates()
            initNewSession()
            
        } else {
            
            self.locationManager.stopAllUpdates()
            finishSession()
        }
    }
    
    func updateDetailsViewLabels(geoData:GeoStateData) {
        
        if self.detailsViewContainer.hidden == false {
            
            self.lngValue.text = String(geoData.longitude)
            self.latValue.text = String(geoData.latitude)
            self.altValue.text = String(geoData.altitude)
            
            self.accxValue.text = String(geoData.x_acc)
            self.accyValue.text = String(geoData.y_acc)
            self.acczValue.text = String(geoData.z_acc)
            
            self.gyroxValue.text = String(geoData.x_gyro)
            self.guroyValue.text = String(geoData.y_gyro)
            self.gyrozValue.text = String(geoData.z_gyro)
            
        }
    }
    
    private func initNewSession() {
        
        DBManager.sharedInstance.insertNewSessionInDB(NSDate())
    }
    
    private func finishSession() {
        
        DBManager.sharedInstance.updateSessionEndTime(SessionManager.sessionNumber, end_time: NSDate())
        SessionManager.sessionNumber = SessionManager.sessionNumber + 1
    }
    
    func setOverlayToMap() {
        
//        self.overlayImage.image = LFHeatMap.heatMapForMapView(self.mapView, boost: 1, locations: locations, weights: weights)
        self.view.layoutSubviews()
    }
    
    func addRoute() {
        
//        let thePath = NSBundle.mainBundle().pathForResource("EntranceToGoliathRoute", ofType: "plist")
//        let pointsArray = NSArray(contentsOfFile: thePath!)
        let pointsCount = locations.count
        var pointsToUse: [CLLocationCoordinate2D] = []
        
        mapView.removeOverlay(self.polyline)
        
        for i in 0...pointsCount-1 {
            
            pointsToUse.append(locations[i].coordinate)
        }
        
        self.polyline = MKPolyline(coordinates: &pointsToUse, count: pointsCount)
        
        mapView.addOverlay(self.polyline, level:  MKOverlayLevel.AboveRoads)

    }
    
    func addActivityAnnotation(at location:CLLocation) {
        
        let annotation = ActivityAnnotation(coordinate: location.coordinate, type: self.locationManager.activity)
        mapView.showAnnotations([annotation], animated: true)
    }
    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        
        let annotationView = ActivityAnnotationView(annotation: annotation, reuseIdentifier: "activity")
        annotationView.canShowCallout = false
        return annotationView
    }
    
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
        
            // draw the track
            let polyLine = overlay
            let polyLineRenderer = MKPolylineRenderer(overlay: polyLine)
            polyLineRenderer.strokeColor = UIColor.blueColor()
            polyLineRenderer.lineWidth = 2.0
            
            return polyLineRenderer
        
    }
}
