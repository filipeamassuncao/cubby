//
//  BaseViewController.swift
//  CUbbY
//
//  Created by Filipe Assunção on 14/03/16.
//  Copyright (c) 2016 Filipe Assunção. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    // MARK: - Initialization
    
    init() {
        
        super.init(nibName: nil, bundle:nil)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil);
        
        if let _ = self.navigationController {
            
            self.navigationController?.navigationBarHidden = false
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - View Initialization
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.proccessBackgroundImage()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func animateBackgroundColorWithColor(color : UIColor) {
        
        UIView.animateWithDuration(0.7) { () -> Void in
            
            self.view.backgroundColor = color
            
        }
    }
    
    private func proccessBackgroundImage() {
        
        UIGraphicsBeginImageContext(self.view.frame.size)
        let _ = UIImage(named: "background_2")?.drawInRect(self.view.bounds)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        self.view.backgroundColor = UIColor(patternImage: image)
    }

}
