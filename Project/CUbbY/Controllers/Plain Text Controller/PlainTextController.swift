//
//  PlainTextController.swift
//  CUbbY
//
//  Created by Filipe Assunção on 30/05/16.
//  Copyright © 2016 Filipe Assunção. All rights reserved.
//

import Foundation
import UIKit

class PlainTextController : BaseViewController {
    
    @IBOutlet weak var textFieldOutlet: UITextView!
    
    override init() {
        
        super.init(nibName:"PlainTextController", bundle: nil)
        self.title = "Plain Text"
        
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        
        self.init()
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        textFieldOutlet.text = ""
//        presentResultsAsPlainText()
    }
    
    override func viewDidAppear(animated: Bool) {
        
        super.viewDidAppear(animated)
        textFieldOutlet.text = ""
//        AnswersAsPlainText()
        presentResultsAsPlainText()
    }
    
    private func AnswersAsPlainText() {
        
        var result = ""
        
        //numero de freguesias e area total
        result += "\n === 1 ===\nDistrito : Coimbra \n"
        result += "nº de freguesias = \(DBManager.sharedInstance.getNumeroFreguesiasFromDistrict("COIMBRA"))\n"
        result += "area de freguesias = \(DBManager.sharedInstance.getAreaFreguesiasFromDistrict("COIMBRA"))\n"
        
        textFieldOutlet.text = result
        
        //sessão do SMC com mais recolhas
        result += "\n === 8 ===\nSessão com mais recolhas \n"
        let maxRecolhas = DBManager.sharedInstance.getSMCSessionWithMorePoints()
        result += "ID de sessão = \(maxRecolhas.session)\n"
        result += "nº de recolhas = \(maxRecolhas.number)\n"
        
        textFieldOutlet.text = result
        
        //actividade com mais recolhas
        result += "\n === 9 ===\nActividade com mais recolhas \n"
        let maxActivity = DBManager.sharedInstance.getGeoStateActivityWithMorePoints()
        result += "Actividade = \(maxActivity.activity)\n"
        result += "nº de recolhas = \(maxActivity.number)\n"
        
        textFieldOutlet.text = result
    }
    
    
    private func presentResultsAsPlainText() {
        
        var result = ""
        
        
        //Session
        var dictionaries = DBManager.sharedInstance.getAllSessionEntries()
        result += "\n === SESSION ===  \(DBManager.sharedInstance.getNumberOfSessions())\n"
        for entry in dictionaries {
            
            result += entry.description + "\n"
        }
        
        //Geo States
        dictionaries = DBManager.sharedInstance.getAllGeoStateEntries()
        result += "\n === GEO STATES === \(DBManager.sharedInstance.getNumberOfGeoStates())\n"
        for entry in dictionaries {
            
            result += entry.description + "\n"
        }
        
        //Routes
        dictionaries = DBManager.sharedInstance.getAllRouteEntries()
        result += "\n === ROUTES === \(DBManager.sharedInstance.getNumberOfRoutes())\n"
        for entry in dictionaries {
            
            result += entry.description + "\n"
        }
        
        textFieldOutlet.text = result
    }
    
}