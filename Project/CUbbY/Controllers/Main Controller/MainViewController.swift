//
//  MainViewController.swift
//  CUbbY
//
//  Created by Filipe Assunção on 11/03/16.
//  Copyright (c) 2016 Filipe Assunção. All rights reserved.
//

import UIKit

typealias cellCallback = () -> ()

class MainViewController: BaseViewController, UITableViewDelegate,UITableViewDataSource {
    
    var optionsArray:[(String,UIImage,cellCallback)] = []
    @IBOutlet weak var optionTableView: UITableView!
    
    convenience override init() {
        
        self.init(nibName: "MainViewController", bundle: nil)
        //initializing the view Controller form specified NIB file
        
        optionsArray = [("Location",UIImage(named: "ww_location")!,buttonPressed),
                        ("Upload",UIImage(named: "settings")!,uploadButtonPressed),
                        ("Settings",UIImage(named: "settings")!,settingsButtonPressed),
                        ("Entries",UIImage(named: "settings")!,entriesButtonPressed),
                        ("Data map",UIImage(named: "settings")!,DataMapButtonPressed)]
        
        self.title = "Main menu"
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.optionTableView.delegate = self
        self.optionTableView.dataSource = self
        self.optionTableView.tableFooterView = UIView(frame: CGRectZero)
        self.optionTableView.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        self.optionTableView.tableHeaderView = UIView(frame: CGRectZero)
        
        self.optionTableView.separatorColor = UIColor.whiteColor()
//        optionTableView.registerClass(MainMenuTableCell.classForCoder(), forCellReuseIdentifier: "cell")
        
        
        let cellNib = UINib(nibName: String(MainMenuTableCell), bundle: NSBundle.mainBundle())
        self.optionTableView.registerNib(cellNib, forCellReuseIdentifier: String(MainMenuTableCell))

    }
    
    func buttonPressed() {
        
        let nextViewController = LocationViewController();
        self.navigationController!.pushViewController(nextViewController, animated: true)
    }
    
    func uploadButtonPressed() {
        
        let nextViewController = UploadViewController()
        self.navigationController!.pushViewController(nextViewController, animated: true)
    }
    
    func settingsButtonPressed() {
        
        let nextViewController = SettingsViewController()
        self.navigationController!.pushViewController(nextViewController, animated: true)
    }
    
    func entriesButtonPressed() {
        
        let nextViewController = PlainTextController()
        self.navigationController!.pushViewController(nextViewController, animated: true)
    }
    
    func DataMapButtonPressed() {
        
        let nextViewController = DataConcentrationController()
        self.navigationController!.pushViewController(nextViewController, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK - TableView Delegate/Datasource
    
    func tableView(tableView: UITableView,
        cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
            
            let cell = tableView.dequeueReusableCellWithIdentifier(String(MainMenuTableCell), forIndexPath: indexPath) as! MainMenuTableCell
            let myItem = optionsArray[indexPath.row]
            
            cell.configureCell(myItem.0, image: myItem.1)
            
            return cell
    }
    
    func tableView(tableView: UITableView,
        numberOfRowsInSection section: Int) -> Int {
            
            return optionsArray.count
    }
    
    func tableView(tableView: UITableView,
        didSelectRowAtIndexPath indexPath: NSIndexPath) {
            
            let callBack:cellCallback = optionsArray[indexPath.row].2
            callBack()
            tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return MainMenuTableCell.defaultHeight()
    }
}
