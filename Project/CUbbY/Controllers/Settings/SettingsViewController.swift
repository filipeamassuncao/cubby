//
//  SettingsViewController.swift
//  CUbbY
//
//  Created by Filipe Assunção on 29/03/16.
//  Copyright © 2016 Filipe Assunção. All rights reserved.
//

import UIKit

class SettingsViewController: BaseViewController {
    
    override init() {
        
        super.init(nibName:"SettingsViewController", bundle: nil)
        self.title = "Settings"
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        
        self.init()
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
