//
//  DataConcentrationController.swift
//  CUbbY
//
//  Created by Filipe Assunção on 30/05/16.
//  Copyright © 2016 Filipe Assunção. All rights reserved.
//

import Foundation

class DataConcentrationController: BaseViewController, MKMapViewDelegate, ModalSettingsControllerDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    var overlayImage:UIImage!
    @IBOutlet weak var overlayImageView: UIImageView!
    @IBOutlet weak var configsButton: UIButton!
    
    lazy var locations                       = DBManager.sharedInstance.getAllGeoStateEntriesAsCLLocations()
    lazy var freguesias                      = DBManager.sharedInstance.getFreguesiasFromCoimbraAsShapeKit()
    lazy var weigths                         = [Double]()
    
    lazy var configsPanel                    = ModalSettingsController()
    
    override init() {
        
        super.init(nibName:"DataConcentrationController", bundle: nil)
        self.title = "Data vizualization"
        
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        
        self.init()
    }
    
    override func viewDidLoad() {
        
        self.mapView.mapType = MKMapType.Hybrid;
        self.mapView.delegate = self
        //        //        var ano = [MKPointAnnotation]()
        //        for _:CLLocation in locations {
        //
        //            //            ano.append(getMePointAnnotation(point))
        //            weigths.append(10)
        //        }
        
        
//        for element:ShapeKitPolygon in freguesias {
//            
//            let polygon:MKPolygon = MKShapeKitPolygon(shapeKitPolygon: element).mapkitShape
//            self.mapView.addOverlay(polygon, level: MKOverlayLevel.AboveRoads)
//        }
        
        self.presentLines()
        self.centerMap()
        self.configsButton.roundCorners([UIRectCorner.BottomRight, UIRectCorner.TopRight, UIRectCorner.TopLeft, UIRectCorner.BottomLeft], radius: 7)
    }
    
    private func presentLines() {
        
        for element:String in DBManager.sharedInstance.getRoutesShapesBySessionAsWKT() {
            
            let polyline:MKPolyline = MKShapeKitPolyline(WKT: element).mapkitShape
            self.mapView.addOverlay(polyline, level: MKOverlayLevel.AboveRoads)
        }
    }
    
    private func getMePointAnnotation(point:ShapeKitPoint) -> MKPointAnnotation {
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = point.coordinate
        return annotation
    }
    
    @IBAction func ConfigsButtonPressed(sender: AnyObject) {
        
        self.configsPanel.delegate = self
        self.presentViewController(self.configsPanel, animated: true, completion: nil)
    }
    
    func centerMap() {
        
        let mapEdgePadding = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        
        mapView.setVisibleMapRect(getMapRectForPoints(locations), edgePadding: mapEdgePadding, animated: true)
    }
    
    func mapView(mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        
        refreshHeatMapOverlay()
    }
    
    func refreshHeatMapOverlay() {
        
        self.overlayImage = LFHeatMap.heatMapForMapView(self.mapView, boost: 1, locations: locations, weights: weigths)
        self.overlayImageView.image = self.overlayImage
    }
    
    func getMapRectForPoints(points:[CLLocation]) -> MKMapRect {
        
        var zoomRect:MKMapRect = MKMapRectNull
        
        for annotation in points {
            
            let aPoint:MKMapPoint = MKMapPointForCoordinate(annotation.coordinate)
            let rect:MKMapRect = MKMapRectMake(aPoint.x, aPoint.y, 0.1, 0.1)
            
            if MKMapRectIsNull(zoomRect) {
                zoomRect = rect
            } else {
                zoomRect = MKMapRectUnion(zoomRect, rect)
            }
        }
        return zoomRect
    }
    
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
        
        if (overlay.isKindOfClass(MKPolyline)) {
            
            return TripOverlayRenderer(polyline: overlay as! MKPolyline, color: UIColor.redColor())
        }
        
        return MKOverlayRenderer()
    }
    
    // Configs methods
    
    func updateDataToActivity(activity:Int) {
        
        if ActivityType.ActivityTypeForIndex(activity) == ActivityType.ActivityTypeNone {
            
            locations = DBManager.sharedInstance.getAllGeoStateEntriesAsCLLocations()
            
        } else {
         
            locations = DBManager.sharedInstance.getGeoStateDataForActivityAsCLLocations(ActivityType.ActivityTypeForIndex(activity))
        }
        
        weigths = [Double]()
        
        for _:CLLocation in locations {

            weigths.append(10)
        }
        
        centerMap()
        refreshHeatMapOverlay()
    }
    
    func showRoutesPolylines(showRoutes:Bool) {
        
        if (showRoutes) {
            
            presentLines()
            
        } else {
            
            self.mapView.removeOverlays(self.mapView.overlays)
        }
    }
}