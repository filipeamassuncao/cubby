//
//  NavigationController.swift
//  CUbbY
//
//  Created by Filipe Assunção on 21/03/16.
//  Copyright © 2016 Filipe Assunção. All rights reserved.
//

import UIKit

enum NavBarComponentsType {
    
    case NavBarComponentTypeFirstItem
    case NavBarComponentTypeFirstItemCustom
    case NavBarComponentTypeSecondItem
    case NavBarComponentTypeSecondItemCustom
    case NavBarComponentTypeThirdItem
    case NavBarComponentTypeThirdItemCustom
    case NavBarComponentTypeFourthItem
    case NavBarComponentTypeFourthItemCustom
    case NavBarComponentTypeFourthItemBarButton
}

enum NavBarNavigationTheme {
    
    case NavBarNavigationThemeDefault
}

@objc protocol NavigationControllerDelegate {
    
    optional func components() -> NSArray
    
    /* @brief  Delegate method to fetch image for first item
    */
    optional func imageForFirstItem() -> UIImage
    
    /* @brief  Delegate method to fetch text for first item
    */
    optional func textForFirstItem() -> String
    
    /* @brief  Delegate method to fetch image for second item
    */
    optional func imageForSecondItem() -> UIImage
    
    /* @brief  Delegate method to fetch text for second item
    */
    optional func textForSecondItem() -> String
    
    /* @brief  Delegate method to fetch imageForRightMenuItem
    */
    optional func imageForThirdItem() -> UIImage
    
    /* @brief  Delegate method to fetch text for third item
    */
    optional func textForThirdItem() -> String
    
    /* @brief  Delegate method to fetch image for fourth item
    */
    optional func imageForFourthItem() -> UIImage
    
    /* @brief  Delegate method to fetch text for fourth item
    */
    optional func textForFourthItem() -> String
    
    /* @brief  Delegate method to fetch if it wants first bar button clicking
    *          Defaults to YES
    *
    *  @return wether the app icon is clickable or not
    */
    optional func allowFirstItemClick() -> Bool
    
    /* @brief  Delegate method to fetch if it wants second bar button clicking
    *          Defaults to YES
    *
    *  @return wether the app icon is clickable or not
    */
    optional func allowSecondItemClick() -> Bool
    
    /* @brief  Delegate method to fetch if it wants third bar button clicking
    *          Defaults to YES
    *
    *  @return wether the app icon is clickable or not
    */
    optional func allowThirdItemClick() -> Bool
    
    /* @brief  Delegate method to fetch if it wants fourth bar button clicking
    *          Defaults to YES
    *
    *  @return wether the app icon is clickable or not
    */
    optional func allowFourthItemClick() -> Bool
    
    /* @brief Delegate method to notify about first item clicked
    */
    optional func firstItemClicked()
    
    /* @brief Delegate method to notify about second item clicked
    */
    optional func secondItemClicked()
    
    /* @brief Delegate method to notify about thrid item clicked
    */
    optional func thirdItemClicked()
    
    /* @brief Delegate method to notify about thrid item clicked
    */
    optional func fourthItemClicked()
    
    /* @brief  Delegate method to fetch a custom view for first item
    */
    optional func customViewForFirstItem() -> UIView
    
    /* @brief  Delegate method to fetch a custom view for second item
    */
    optional func customViewForThirdItem() -> UIView
    
    /* @brief  Delegate method to fetch a custom view for thrid item
    */
    optional func customViewForFourthItem() -> UIView
    
    /* @brief  Delegate method to add an uibarbutton item for fourth item
    */
    optional func barButtonItemForFourthItem() -> UIBarButtonItem
}

class NavigationController: UINavigationController {

    var navigationDelegate:NavigationControllerDelegate?
    var navigationTheme:NavBarNavigationTheme
    private var navigationComponents:NSArray
    
    init(rootController:UIViewController) {
        
        navigationTheme = .NavBarNavigationThemeDefault
        navigationComponents = NSArray()
        super.init(rootViewController: rootController)
    }

    required init?(coder aDecoder: NSCoder) {
        
        navigationTheme = .NavBarNavigationThemeDefault
        navigationComponents = NSArray()
        super.init(coder: aDecoder)

    }
    
    
    override func viewDidLoad() {
        
        styleBar()
    }
    
    private func styleBar() {
        
        for obj : AnyObject in navigationComponents {
            
            if let type = obj as? NavBarComponentsType {
                
                addItemOfType(type)
                
            }
        }
    }
    
    private func addItemOfType(type:NavBarComponentsType) {
        
        switch type {
            
        case .NavBarComponentTypeFirstItem:
            break
            
        default: break
            
        }
    }
    
    //MARK: Private - items configuration
    
//    private func setupFirstItem() {
//    
//        var leftBarButtonItem:UIBarButtonItem;
//        
//        if let itemImage = getMeFirstItemImage() {
//            
//      
    
//    if([self firstItemImage]) {
//    
//    UIImage * itemImage = [[self firstItemImage] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
//    
//    leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:itemImage
//    style:UIBarButtonItemStylePlain
//    target:self action:@selector(firstItemClicked:)];
//    
//    } else if([self firstItemText]) {
//    
//    leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:[self firstItemText]
//    style:UIBarButtonItemStylePlain
//    target:self action:@selector(firstItemClicked:)];
//    }
//    
//    if (![self firstItemClickedAllowed]) {
//    
//    leftBarButtonItem.target = nil;
//    }
//    
//    [self addToLeftNavigationItemsAnUIBarButtonItem:leftBarButtonItem];
//    }
    
    //MARK: Private - items elements
    
    func getMeFirstItemImage() -> UIImage? {
        
        if let _ = navigationDelegate {
            
            return navigationDelegate!.imageForFirstItem!()
        }
        return nil
    }
}
