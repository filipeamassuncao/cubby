//
//  SQLHelper+SHPImporter.swift
//  CUbbY
//
//  Created by Filipe Assunção on 07/06/16.
//  Copyright © 2016 Filipe Assunção. All rights reserved.
//

import Foundation

extension SQLHelper {
    
    private static func importShapeFileToDatabase(filename:String, database:SpatialDatabase) -> Bool {
        
        if let path:String = NSBundle.mainBundle().pathForResource(filename, ofType: "shp") {
            
            let realPath = path.stringByReplacingOccurrencesOfString(".shp", withString: "")
            
            let createVirtualTable = "CREATE VIRTUAL TABLE \(filename) USING VirtualShape('\(realPath)',CP1252,4326);"
            
            if database.executeStatements(createVirtualTable) {
                
//                database.executeStatements("UPDATE geometry_columns SET srid = 4326 WHERE f_table_name = '\(filename)';")
//                database.executeStatements("UPDATE \(filename) SET Geometry = SetSRID(Geometry, 4326);")
                
                let rows = getNumberOfTotalRowsInTable(filename, database: database)
                NSLog("[SHP_IMPORTER] LOADED \(rows) ROWS TO TABLE \(filename)")
                return true
                
            } else {
                
                return false
            }
        }
        return false
    }
    
    static func initVirtualTablesFromShapeFile(database:SpatialDatabase) {
        
        importShapeFileToDatabase("adminareas_general", database: database)
        importShapeFileToDatabase("boundaries", database: database)
        importShapeFileToDatabase("adminareas", database: database)
        importShapeFileToDatabase("roads", database: database)
        importShapeFileToDatabase("freguesias_PT", database: database)

    }
    
    static func testing(database:SpatialDatabase) {
        
        let query = "SELECT * FROM roads"
        
        let rs = database.executeQuery(query, withArgumentsInArray:nil)
        while (rs.next()) {
            
            NSLog("\(rs.resultDictionary())")
        }
    }

}