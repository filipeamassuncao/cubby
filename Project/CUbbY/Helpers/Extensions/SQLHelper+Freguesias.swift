//
//  SQLHelper+Freguesias.swift
//  CUbbY
//
//  Created by Filipe Assunção on 09/06/16.
//  Copyright © 2016 Filipe Assunção. All rights reserved.
//

import Foundation

let kSQLiteAreaFreguesiasPorDistrito           = "SELECT SUM(view_areas.area_freguesia) as area_total FROM (SELECT ST_Area(Geometry) AS area_freguesia FROM freguesias_PT WHERE freguesias_PT.Distrito = \"%@\") as view_areas"

let kSQLiteNumeroFreguesiasPorDistrito         = "SELECT COUNT(*) as numero FROM freguesias_PT WHERE freguesias_PT.Distrito = \"%@\""

extension SQLHelper {
    
    
    static func getFreguesiasFromCoimbraAsShape(database:SpatialDatabase) -> [ShapeKitPolygon] {
        
        let queryStatement = "SELECT Geometry AS freguesia FROM freguesias_PT WHERE freguesias_PT.Distrito = \"COIMBRA\""
        var freguesias = [ShapeKitPolygon]()
        
        
        let rs = database.executeQuery(queryStatement, withArgumentsInArray:nil)
        while (rs.next()) {
            
            if let element = rs.objectForColumnName("freguesia") as! ShapeKitPolygon! {
                
                freguesias.append(element)
            }
        }
        return freguesias
    }
    
    static func getDistricts(database:SpatialDatabase) -> [String] {
        
        let queryStatement = "SELECT DISTINCT Distrito AS distrito FROM freguesias_PT"
        var districts = [String]()
        
        
        let rs = database.executeQuery(queryStatement, withArgumentsInArray:nil)
        while (rs.next()) {
            
            if let element = rs.stringForColumn("distrito") {
                
                districts.append(element)
            }
        }
        return districts
    }
    
//    static func getDistrictsAndFreguesias(database:SpatialDatabase) -> [(name:String, freguesias:[String])] {
//        
//        let queryStatement = "SELECT Distrito FROM freguesias_PT"
//        var districts = [(name:String, freguesias:[String])]()
//        
//        
//        let rs = database.executeQuery(queryStatement, withArgumentsInArray:nil)
//        while (rs.next()) {
//            
//            if let element = rs.stringForColumn("distrito") {
//                
////                districts.append(element)
//            }
//        }
//        return districts
//    }
    
    static func getAreaFreguesiaByDistrict(database:SpatialDatabase, district:String) -> String {
        
        let rs = database.executeQuery(String(format: kSQLiteAreaFreguesiasPorDistrito, arguments: [district]), withArgumentsInArray:nil)
        while (rs.next()) {
            
            if let element = rs.stringForColumn("area_total") {
                 
                return element
            }
        }
        return ""
    }
    
    static func getNumeroFreguesiasByDistrict(database:SpatialDatabase, district:String) -> String {
        
        let rs = database.executeQuery(String(format: kSQLiteNumeroFreguesiasPorDistrito, arguments: [district]), withArgumentsInArray:nil)
        while (rs.next()) {
            
            if let element = rs.stringForColumn("numero") {
                
                return element
            }
        }
        return ""
    }
    
    static func getAreaForDistrict(database:SpatialDatabase, district:String) -> String {
        
        let getFreguesias         = "SELECT ST_Area(distrito.uniao) AS area_distrito FROM (SELECT ST_Union(freguesias.Geometry) AS uniao FROM (SELECT Geometry FROM freguesias_PT WHERE freguesias_PT.Distrito = \"%@\") AS freguesias) distrito"
        let rs = database.executeQuery(String(format: getFreguesias, arguments: [district]), withArgumentsInArray:nil)
        while (rs.next()) {
            
            return rs.stringForColumn("area_distrito")
        }
        return ""
    }
    
    static func getRoadsByDistrict(database:SpatialDatabase, district:String) -> String {
        
//        let freguesias =     "SELECT ST_Union(freguesias.Geometry) AS Geometry FROM (SELECT Geometry FROM freguesias_PT WHERE freguesias_PT.Distrito = \"COIMBRA\") AS freguesias"
        
//        let getFreguesias         = "(SELECT ST_Union(freguesias.Geometry) AS distrito FROM (SELECT Geometry FROM freguesias_PT WHERE freguesias_PT.Distrito = \"%@\") AS freguesias) AS d"
        
        let teste = "SELECT SUM(ST_Length(r.Geometry)) AS length FROM roads AS r, freguesias_PT AS d WHERE d.Distrito = 'COIMBRA' AND ST_Intersects(r.Geometry, d.Geometry)"
        
        let rs = database.executeQuery(teste, withArgumentsInArray:nil)
        while (rs.next()) {
            
            NSLog("\(rs.resultDictionary())")
//            if let element = rs.stringForColumn("numero") {
//                
//                return element
//            }
        }
        return ""
    }
}