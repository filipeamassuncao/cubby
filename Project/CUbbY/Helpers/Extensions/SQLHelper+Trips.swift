//
//  SQLHelper+Trips.swift
//  CUbbY
//
//  Created by Filipe Assunção on 12/06/16.
//  Copyright © 2016 Filipe Assunção. All rights reserved.
//

import Foundation


private let kSQLiteCreateTableRoutes          = "CREATE TABLE IF NOT EXISTS routes ( id INTEGER PRIMARY KEY AUTOINCREMENT, session_id INTEGER, FOREIGN KEY(session_id) REFERENCES sessions(id))"

private let kSQLiteGetAllFromRoutes           = "SELECT id, session_id, geom_line FROM routes"

private let kSQLiteSelectCountRoutesRows     = "SELECT COUNT(*) FROM routes"

extension SQLHelper {
    
    static func createRoutesTable(database:SpatialDatabase) -> Bool {
        
        if database.executeStatements(kSQLiteCreateTableRoutes) {
            
            return createTableRoutesGeometry(database)
            
        } else {
            
            return false
        }
    }
    
    static func createTableRoutesGeometry(database:SpatialDatabase) -> Bool {
        
        let addLineStringCollumn = "SELECT AddGeometryColumn('routes', 'geom_line', 4326, 'LINESTRING', 'XY');ANALYZE;"
        
        if (database.executeStatements(addLineStringCollumn)) {
            
            let insertGeomPointTrigger = "CREATE TRIGGER create_route AFTER UPDATE OF end_time ON sessions BEGIN INSERT INTO routes (session_id, geom_line) VALUES (new.id, (SELECT MakeLine(points) as route FROM (SELECT geostates.geom_point AS points FROM geostates WHERE geostates.session_id = new.id ORDER BY geostates.date))); END;"
            
            return database.executeStatements(insertGeomPointTrigger)
            
        } else {
            
            return false
        }
    }
    
    static func getRoutesEntries(database:SpatialDatabase) -> FMResultSet {
        
        let rs = database.executeQuery(kSQLiteGetAllFromRoutes, withArgumentsInArray: nil)
        
        return rs
    }
    
    static func getRoutesBySession(database:SpatialDatabase) -> [ShapeKitPolyline] {
        
        var lines = [ShapeKitPolyline]()
        
        let getRoutesOrderedBySessionQuery = "SELECT routes.geom_line AS route FROM routes ORDER BY session_id"
        let rs = database.executeQuery(getRoutesOrderedBySessionQuery, withArgumentsInArray:nil)
        
        if let element = rs.objectForColumnName("route") as! ShapeKitPolyline! {
            
            lines.append(element)
        }
        
        return lines
    }
    
    static func getRoutesBySessionAsWKT(database:SpatialDatabase) -> [String] {
        
        var lines = [String]()
        
        let getRoutesOrderedBySessionQuery = "SELECT AsText(routes.geom_line) as route FROM routes ORDER BY routes.session_id"
        let rs = database.executeQuery(getRoutesOrderedBySessionQuery, withArgumentsInArray:nil)
        
        while (rs.next()) {
            
            if let element = rs.stringForColumn("route") {
                
                lines.append(element)
            }
        }
        return lines
    }
    
    static func getNumberOfTotalRoutes(database:SpatialDatabase) -> Int {
        
        let rs = database.executeQuery(kSQLiteSelectCountRoutesRows, withArgumentsInArray:nil)
        while (rs.next()) {
            
            return Int(rs.intForColumn("COUNT(*)"))
        }
        return 1
    }
}