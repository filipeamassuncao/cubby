//
//  SQLHelper+SenseMyCity.swift
//  CUbbY
//
//  Created by Filipe Assunção on 04/06/16.
//  Copyright © 2016 Filipe Assunção. All rights reserved.
//

import Foundation


private let kSQLiteSelectLocationFromCoimbraGPS      = "SELECT lat, lon, alt FROM coimbra_gps"

private let kSQLiteSelectSessionsWithPoints          = "SELECT session_id, COUNT(session_id) AS numberSessions FROM (SELECT DISTINCT session_id AS session FROM coimbra_gps) as sessions, coimbra_gps WHERE coimbra_gps.session_id = sessions.session GROUP BY sessions.session ORDER BY numberSessions DESC"

private let kSQLiteSelectSessionWithMaxPoints        = "SELECT t.session_id, MAX(t.numberSessions) AS max FROM (SELECT session_id, COUNT(session_id) AS numberSessions FROM (SELECT DISTINCT session_id AS session FROM coimbra_gps) as sessions, coimbra_gps WHERE coimbra_gps.session_id = sessions.session GROUP BY sessions.session ORDER BY numberSessions DESC) as t"

extension SQLHelper {
    
    static func getNumberOfTotalSessionsFromSMC(database:SpatialDatabase) -> Int {
        
        let countSessions = "SELECT COUNT(DISTINCT session_id) AS session FROM coimbra_gps"
        
        let rs = database.executeQuery(countSessions, withArgumentsInArray:nil)
        while (rs.next()) {
            
            return Int(rs.intForColumn("sessionsNumber"))
        }
        return 0
    }
    
    static func test(database:SpatialDatabase) {
        
        let pressureData = "SELECT gps.lat, gps.lon, gps.seconds AS gps_s, pressure.pre_data AS pre_data FROM coimbra_gps AS gps LEFT OUTER JOIN coimbra_pressure AS pressure ON gps.gpstime = pressure.seconds"
        
        let rs = database.executeQuery(pressureData, withArgumentsInArray:nil)
        while (rs.next()) {
            
            NSLog("\(rs.resultDictionary())")
        }
    }
    
    static func getSenseMyCityCoimbraGPS(database:SpatialDatabase) -> FMResultSet {
        
        let rs = database.executeQuery(kSQLiteSelectLocationFromCoimbraGPS, withArgumentsInArray: nil)
        return rs
    }
    
    static func getSMCSessionWithMorePoints(database:SpatialDatabase) -> (session:Int, number:Int) {
        
        let rs = database.executeQuery(kSQLiteSelectSessionWithMaxPoints, withArgumentsInArray:nil)
        while (rs.next()) {
            
            return (Int(rs.intForColumn("session_id")), Int(rs.intForColumn("max")))
        }
        return (0,0)
    }
}