//
//  SQLHelper+CSVImporter.swift
//  CUbbY
//
//  Created by Filipe Assunção on 03/06/16.
//  Copyright © 2016 Filipe Assunção. All rights reserved.
//

import Foundation

extension SQLHelper {
    
    private static func importCSVFileToDatabase(filename:String, database:SpatialDatabase) -> Bool {
        
        if let path:String = NSBundle.mainBundle().pathForResource(filename, ofType: "csv") {
            
            let createVirtualTable = "CREATE VIRTUAL TABLE \(filename) USING VirtualText('\(path)',CP1252, 1, POINT, DOUBLEQUOTE, ',');"
            
            if database.executeStatements(createVirtualTable) {
                
                database.executeStatements("ANALYZE;")
                let rows = getNumberOfTotalRowsInTable(filename, database: database)
                NSLog("[CSV_IMPORTER] LOADED \(rows) ROWS TO TABLE \(filename)")
                return true
                
            } else {
                
                return false
            }
        }
        return false
    }
    
    static func initVirtualTablesFromCSV(database:SpatialDatabase) {
        
        importCSVFileToDatabase("coimbra_gps", database: database)
        importCSVFileToDatabase("coimbra_pressure", database: database)
        initVirtualTablesFromShapeFile(database)

    }
    
    static func crossGPSwithPressureData(database:SpatialDatabase) {
        
        let countSessions = "SELECT COUNT(DISTINCT session_id) FROM coimbra_gps"
        
        let rs2 = database.executeQuery(countSessions, withArgumentsInArray:nil)
        while (rs2.next()) {
            
            NSLog("\(Int(rs2.intForColumn("COUNT(*)")))")
        }
    }
}