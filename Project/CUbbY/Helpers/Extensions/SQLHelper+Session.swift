//
//  SQLHelper+Session.swift
//  CUbbY
//
//  Created by Filipe Assunção on 03/06/16.
//  Copyright © 2016 Filipe Assunção. All rights reserved.
//

import Foundation



private let kSQLiteCreateTableSessions         = "CREATE TABLE IF NOT EXISTS sessions ( id INTEGER PRIMARY KEY AUTOINCREMENT, start_time DATE NOT NULL, end_time DATE)"


private let kSQLiteSelectAllFromSessions       = "SELECT id, start_time, end_time FROM sessions"

private let kSQLiteSelectCountSessionsRows      = "SELECT COUNT(*) FROM sessions"

private let kSQLiteInsertSession                = "INSERT INTO sessions (start_time) VALUES (?)"

private let kSQLiteUpdateSessionEndTime         = "UPDATE sessions SET end_time = ? WHERE ID = ?"


extension SQLHelper {
    
    
    static func createSessionsTable(database:SpatialDatabase) -> Bool {
        
        if database.executeStatements(kSQLiteCreateTableSessions) {
            
            return true
            
        } else {
            
            return false
        }
    }
    
    static func insertSession(start_date:NSDate, database:SpatialDatabase) -> Bool {
        
        if database.executeUpdate(kSQLiteInsertSession, withArgumentsInArray: [start_date]) {
            
            return true
        }
        return false
    }
    
    static func updateSessionEndTime(session:Int, end_time:NSDate, database:SpatialDatabase) -> Bool {
        
        if database.executeUpdate(kSQLiteUpdateSessionEndTime, withArgumentsInArray: [end_time, session]) {
            
            return true
        }
        return false
    }
    
    
    static func getSessionEntries(database:SpatialDatabase) -> FMResultSet {
        
        let rs = database.executeQuery(kSQLiteSelectAllFromSessions, withArgumentsInArray: nil)
        
        return rs
    }
    
    static func getNumberOfTotalSessions(database:SpatialDatabase) -> Int {
        
        let rs = database.executeQuery(kSQLiteSelectCountSessionsRows, withArgumentsInArray:nil)
        while (rs.next()) {
            
            return Int(rs.intForColumn("COUNT(*)"))
        }
        return 1
    }
}