//
//  SQLHelper+GeoState.swift
//  CUbbY
//
//  Created by Filipe Assunção on 03/06/16.
//  Copyright © 2016 Filipe Assunção. All rights reserved.
//

import Foundation


private let kSQLiteCreateTableGeoStates             = "CREATE TABLE IF NOT EXISTS geostates ( id INTEGER PRIMARY KEY AUTOINCREMENT,longitude DOUBLE NOT NULL,latitude DOUBLE NOT NULL, altitude DOUBLE NOT NULL, date DATE, x_acc DOUBLE NOT NULL, y_acc DOUBLE NOT NULL, z_acc DOUBLE NOT NULL, x_gyro DOUBLE NOT NULL, y_gyro DOUBLE NOT NULL, z_gyro DOUBLE NOT NULL, proximity DOUBLE NOT NULL, activity VARCHAR(20) NOT NULL, session_id INTEGER, FOREIGN KEY(session_id) REFERENCES sessions(id));"


private let kSQLiteSelectAllFromGeoStates           = "SELECT longitude, latitude, altitude, date, x_acc, y_acc, z_acc, x_gyro, y_gyro, z_gyro, proximity, activity, geom_point, session_id FROM geostates"

private let kSQLiteSelectByActivity                 = "SELECT * FROM geostates WHERE activity = \"%@\""

private let kSQLiteSelectCountGeoStatesRows         = "SELECT COUNT(*) FROM geostates"


private let kSQLiteInsertGeoState                   = "INSERT INTO geostates (longitude, latitude, altitude, date, x_acc, y_acc, z_acc, x_gyro, y_gyro, z_gyro, proximity, activity, session_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"

private let kSQLiteSelectPointsFromSessionAsText    = "SELECT AsText(geom_point) AS point FROM geostates WHERE geostates.session_is = ? ORDER BY timestamp"

private let kSQLiteSelectPointsActivityFromPoints   = "SELECT activity, COUNT(activity) as number_points FROM geostates GROUP BY geostates.activity ORDER BY number_points DESC LIMIT 1"


extension SQLHelper {
    
    static func createGeoStateTable(database:SpatialDatabase) -> Bool {
        
        if database.executeStatements(kSQLiteCreateTableGeoStates) {
            
            return createSpatialTablePoints(database)
            
        } else {
            
            return false
        }
    }
    
    
    static func createSpatialTablePoints(database:SpatialDatabase) -> Bool {
        
        let addPointCollumn = "SELECT AddGeometryColumn('geostates', 'geom_point', 4326, 'POINT', 'XY');ANALYZE;"
        
        if (database.executeStatements(addPointCollumn)) {
            
            let insertGeomPointTrigger = "CREATE TRIGGER insert_geom_point AFTER INSERT ON geostates BEGIN UPDATE geostates SET geom_point = MAKEPOINT(new.longitude,new.latitude, 4326) WHERE geostates.session_id = new.session_id AND geostates.longitude = new.longitude AND geostates.latitude = new.latitude ; END;"
            
            return database.executeStatements(insertGeomPointTrigger)
            
        } else {
            
            return false
        }
    }
    
    static func getAllGeoStatesByActivity(activityType:ActivityType, database:SpatialDatabase) -> FMResultSet {
        
        let queryStatement = String(format: kSQLiteSelectByActivity, activityType.rawValue)
        return database.executeQuery(queryStatement, withArgumentsInArray: nil)
    }
    
    
    static func insertGeoStatePoint(database:SpatialDatabase, data:GeoStateData) -> Bool {
        
        let args = [data.longitude,data.latitude,data.altitude, data.timestamp, data.x_acc,data.y_acc,data.z_acc, data.x_gyro, data.y_gyro, data.z_gyro, data.optional, data.activity, SessionManager.sessionNumber]
        if database.executeUpdate(kSQLiteInsertGeoState, withArgumentsInArray: args) {
            
            return true
        }
        return false
    }
    
    static func getGeoStateEntries(database:SpatialDatabase) -> FMResultSet {
        
        let rs = database.executeQuery(kSQLiteSelectAllFromGeoStates, withArgumentsInArray: nil)
        return rs
    }
    
    static func getNumberOfTotalGeoStates(database:SpatialDatabase) -> Int {
        
        let rs = database.executeQuery(kSQLiteSelectCountGeoStatesRows, withArgumentsInArray:nil)
        while (rs.next()) {
            
            return Int(rs.intForColumn("COUNT(*)"))
        }
        return 1
    }
    
    static func getActivityWithMorePoints(database:SpatialDatabase) -> (activity:String, number:Int) {

        let rs = database.executeQuery(kSQLiteSelectPointsActivityFromPoints, withArgumentsInArray:nil)
        while (rs.next()) {
            
            return (rs.stringForColumn("activity"), Int(rs.intForColumn("number_points")))
        }
        return ("",0)
    }
}