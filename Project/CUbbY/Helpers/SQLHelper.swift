//
//  SQLHelper.swift
//  CUbbY
//
//  Created by Filipe Assunção on 24/05/16.
//  Copyright © 2016 Filipe Assunção. All rights reserved.
//

// ==== SQLite ==== //
// CREATE TABLES


// SELECT

private let kSQLiteSelectAllFromGeneric        = "SELECT * FROM %@"

private let kSQLiteSelectCountGenericRows      = "SELECT COUNT(*) FROM %@"

// INSERT



// UPDATE

import Foundation

class SQLHelper {

    static func getAllEntriesInTable(table:String , database:SpatialDatabase) -> FMResultSet {
        
        
        let queryString = String(format: kSQLiteSelectAllFromGeneric, arguments: [table])
        return database.executeQuery(queryString, withArgumentsInArray:nil)
        
    }
    
    static func getNumberOfTotalRowsInTable(table:String, database:SpatialDatabase) -> Int {
        
        let queryString = String(format: kSQLiteSelectCountGenericRows, arguments: [table])
        let rs = database.executeQuery(queryString, withArgumentsInArray:nil)
        while (rs.next()) {
            
            return Int(rs.intForColumn("COUNT(*)"))
        }
        return 0
    }
    
    static func printTable(table:String, database:SpatialDatabase) {
        
        let queryString = String(format: kSQLiteSelectAllFromGeneric, arguments: [table])
        let rs = database.executeQuery(queryString, withArgumentsInArray:nil)
        while (rs.next()) {
            
            NSLog("\(rs.resultDictionary())")
        }
    }
    
    static func printStatementResult(statement:String, database:SpatialDatabase) {
        
        let rs = database.executeQuery(statement, withArgumentsInArray:nil)
        while (rs.next()) {
            
            NSLog("\(rs.resultDictionary())")
        }
    }
}