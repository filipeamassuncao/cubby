//
//  GeoStateData.swift
//  CUbbY
//
//  Created by Filipe Assunção on 08/04/16.
//  Copyright © 2016 Filipe Assunção. All rights reserved.
//

import Foundation

class GeoStateData {
    
//    lat lng alt time x_acc y_acc z_acc x_gyro y_gyro z_gyro OptionalSensor activity
    let latitude:Double
    let longitude:Double
    let altitude:Double
    let timestamp:NSDate
    let x_acc:Double
    let y_acc:Double
    let z_acc:Double
    let x_gyro:Double
    let y_gyro:Double
    let z_gyro:Double
    let optional:Double
    let activity:String
    
    
    static func getDate() -> NSDate {
        
        return NSDate()
    }
    
    private static func getDateFromString(date:String) -> NSDate {
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.locale = NSLocale.currentLocale()
        return dateFormatter.dateFromString(date)!
    }
    
    init(lat:Double, lon:Double, alt:Double, time:NSDate, xacc:Double, yacc:Double,zacc:Double,xgyro:Double,ygyro:Double,zgyro:Double,opt:Double,act:String) {
        
        latitude = lat
        longitude = lon
        altitude = alt
        timestamp = time
        x_acc = xacc
        y_acc = yacc
        z_acc = zacc
        x_gyro = xgyro
        y_gyro = ygyro
        z_gyro = zgyro
        optional = opt
        activity = act
    }
    
    func getOrderedArray() -> NSArray {
        
        return NSArray(objects: [latitude,longitude,altitude,timestamp,x_acc,y_acc,z_acc,x_gyro,y_gyro,z_gyro,optional,activity]);
    }
    
    func printMe() -> String {
        
        return "GeoStateData : \n(\(latitude),\(longitude))\nAlt : \(altitude)\nAcc : (\(x_acc):\(y_acc):\(z_acc)\nGyro : (\(x_gyro):\(y_gyro):\(z_gyro))"
        
    }
    
    static func buildWithOrderedArray(array:[String]) -> GeoStateData {
        
        return GeoStateData(lat: Double(array[0])!,
            lon: Double(array[1])!,
            alt: Double(array[2])!,
            time: GeoStateData.getDateFromString(array[3]),
            xacc: Double(array[4])!,
            yacc: Double(array[5])!,
            zacc: Double(array[6])!,
            xgyro: Double(array[7])!,
            ygyro: Double(array[8])!,
            zgyro: Double(array[9])!,
            opt: Double(array[10])!,
            act: array[11])
    }
}