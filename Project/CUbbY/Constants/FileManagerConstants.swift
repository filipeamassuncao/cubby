//
//  FileManagerConstants.swift
//  CUbbY
//
//  Created by Filipe Assunção on 31/03/16.
//  Copyright © 2016 Filipe Assunção. All rights reserved.
//

import Foundation

class FileManagerConstants {
    
    let kFileManagerXACCField               = "x_acc"
    let kFileManagerYACCField               = "y_acc"
    let kFileManagerZACCField               = "z_acc"
    let kFileManagerXGYROField              = "x_gyro"
    let kFileManagerYGYROField              = "y_gyro"
    let kFileManagerZGYROField              = "z_gyro"
    let kFileManagerLatitudeField           = "lat"
    let kFileManagerLongitudeField          = "lng"
    let kFileManagerAltitudeField           = "alt"
    let kFileManagerTimeField               = "time"
    let kFileManagerActivityField           = "activity"
}